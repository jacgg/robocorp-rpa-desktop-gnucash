*** Settings ***
Documentation   Automatizacion Desktop
Library         RPA.Desktop.Windows
Library         RPA.Desktop
Library         RPA.Excel.Files
Suite Setup     Open the ERP desktop GnuCash desktop
Suite Teardown  Cerrar GnuCash

*** Variables ***
${EXECUTABLE_gnucash}=     C:/Program Files (x86)/gnucash/bin/gnucash.exe
${WINDOW_TITLE_gnucash}=    test.gnucash - Cuentas - GnuCash

*** Keywords ***
Open the ERP desktop GnuCash desktop
    Open Executable   ${EXECUTABLE_gnucash}   ${WINDOW_TITLE_gnucash}

*** Keywords ***
Cerrar GnuCash
    Click   alias:button.cerrar
    Send Keys   ^q
    Click   alias:button.cerrar-sin-guardar

*** Keywords ***
Ir a crear nuevo Cliente
    Sleep   2s
    Click       alias:button.negocios
    Sleep   2s
    Press Keys    down
    Sleep   1s
    Press Keys    right
    Press Keys    enter
    Sleep   1s
    Click   alias:button.crear.cliente

*** Keywords ***
Ingreso de registros
    Open Workbook    clientes.xlsx
    ${registros}=    Read Worksheet As Table    header=True
    Close Workbook
    
    FOR    ${registro}    IN    @{registros}
        Ingresar datos para un registro     ${registro}
    END
    Click       alias:button.cancel

*** Keywords ***
Ingresar datos para un registro
    [arguments]  ${registro}
    Type Text   ${registro}[Nombre_sociedad]
    Press Keys    tab  
    Press Keys    tab 
    Type Text   ${registro}[Nombre]
    Press Keys    tab
    Type Text   ${registro}[Address1]
    Press Keys    tab
    Type Text   ${registro}[Address2]
    Press Keys    tab
    Type Text   ${registro}[Address3]
    Press Keys    tab
    Type Text   ${registro}[Address4]
    Press Keys    tab
    Type Text   ${registro}[Phone_Number]
    Press Keys    tab
    Type Text   ${registro}[Fax]
    Press Keys    tab
    Type Text   ${registro}[Mail]
    Press Keys    tab
    Type Text   ${registro}[Note]
    
    Click       alias:button.aceptar
    Sleep       2s
    Click       alias:button.crear.cliente 


*** Tasks ***
Minimal task
    Ir a crear nuevo Cliente
    Ingreso de registros


